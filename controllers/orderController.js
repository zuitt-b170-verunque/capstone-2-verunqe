const User = require ("../models/User.js")
const Product = require ("../models/Product.js")
const Order = require ("../models/Order.js")

const auth = require("../auth.js")
const bcrypt = require("bcrypt")




// NON-ADMIN USER CHECK-OUT / ORDER
module.exports.order =  (reqBody, userData) => {
    console.log(`this is what's inside reqBody`)
        console.log(reqBody)
        console.log(`this is what's inside userData`)
        console.log(userData)

    return User.find(userData.userId).then(result => {
        if(userData.isAdmin===true){
            return `You are an Admin you can't buy your own stuff`
        } else {

            console.log(userData.id)
            let updateUser =  User.findById(userData.id).then(user => {
                // add to CART
                console.log(user.cart)
                user.cart.push({productId:reqBody.productId, totalAmount: reqBody.totalAmount, isPaid: reqBody.isPaid })
                
                // saving
                return user.save().then((user, err) => {
                if (err) {
                    return false
                } else {
                        return true
                    }
                })
            })


            // add to ORDER
            let newOrder = new Order ({
                totalAmount: reqBody.totalAmount,
                isPaid: reqBody.isPaid
            })

            return newOrder.save().then((saved, error) => {
                if (error) {
                    console.log(error)
                    return false
                } else {
                    return true
                }
            })
        }
    })
}




// ADD TO CART
module.exports.cart =  (reqBody, userData) => {

    return   User.find(userData.userId).then (result => {
        if(userData.isAdmin===true){
            return `You are an Admin you can't buy your own stuff`
        } else {
            console.log(userData.id)
            let updateUser =   User.findById(userData.id).then(user => {
                // add to CART
                console.log(user.cart)
                user.cart.push({productId:reqBody.productId })
                
            })
        }
    })
}






// RETRIEVE AUTHENTICATED USER'S ORDER
module.exports.retrieveOrder =  (reqBody, userData) => {
    console.log(`this is what's inside reqBody`)
        console.log(reqBody)
        console.log(`this is what's inside userData`)
        console.log(userData)

    return User.find(userData.userId).then(result => {
        if(userData.isAdmin===true){
            return `You are an Admin you can't buy your own stuff`
        } else {
            console.log(userData.id)
            return User.findById(userData.id).then((result, error) => {
                if (error) {
                    console.log (error)
                    return false
                } else {
                    console.log(result.cart)
                    return result.cart
                }
            })
        }
    })
}




// RETRIEVE ALL ORDERS (ADMIN)
module.exports.retrieveAllOrder = (userData) => {
    console.log(userData)
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin === false) {
            return "Only Admin can use this function"
        } else {
            return Order.find( {} ).then((result, error) => {
                if (error) {
                    return false
                }else{
                    return result
                }
            })
        }
    })
}