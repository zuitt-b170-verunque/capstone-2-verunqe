const User = require ("../models/User.js")
const Product = require ("../models/Product.js")
const Order = require ("../models/Order.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")
const router = require("../routes/userRoutes.js")




// RETRIEVE ALL ACTIVE PRODUCTS
module.exports.getActiveProducts = () => {
    return Product.find ({isActive:true}).then((result, error) => {
        if (error){
            console.log(error)
            return false
        } else {
            return result
        }
    })
}

// GET ALL
module.exports.getAllProducts = () => {
	return Product.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}



// RETRIEVE A SINGLE PRODUCT
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams).then(result => {
        return result
    })
}

// CREATE A PRODUCT (ADMIN PRIVILAGE)
module.exports.addItem = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin === false) {
            return "Only Admin can Add Items"
        } else {
            let newProduct = new Product (
                {
                name : reqBody.name,
                description : reqBody.description,
                price : reqBody.price
                }
            )
            return newProduct.save().then((saved, error)=>{
                if (error){
                    console.log(error)
                    return false
                } 
                else {
                    return true
                }
            })
                /* return Product.find ({}).then((result, error) => {
                // let checkItem =result[i].name
                // console.log(checkItem)
                // let checkItem = result.name
                // console.log(checkItem)
                if (result !== null){
                    return `Duplicate Item`
                } else {
                    // console.log(result)            
                }
                }) */
        }
    })
}


// UPDATE PRODUCT INFO
module.exports.updateProduct = (reqParams, reqBody, userData) => {
    return User.findById(userData.userId).then((result, error) => {
        if (error) {
            return false
        } else {
          if (userData.isAdmin === false) {
              return false
          } else {
            let updatedProduct = {
                name : reqBody.name,
                description : reqBody.description,
                price : reqBody.price
            }
            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
                if (error) {
                    return false
                } else	{
                    return true
                }
            })
          }
        }
    })
}



// ARCHIVE PRODUCT (ADMIN PRIVILAGE)
module.exports.archiveProduct = (reqParams, reqBody, userData ) => {
    return User.findById(userData.userId).then((result, error) => {
        if (error) {
            return false
        } else {
          if (userData.isAdmin === false) {
              return false
          } else {
            let archiveProduct = {
                isActive : reqBody.isActive
            }
            return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((result, error) => {
                if (error) {
                    return false
                } else	{
                    return true
                }
            })
          }
        }
    })
}

