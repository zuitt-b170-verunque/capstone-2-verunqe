const User = require ("../models/User.js")
const Products = require ("../models/Product.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")


// user registration
module.exports.registerUser = (reqBody) => {
    let newUser = new User ({
        email : reqBody.email,
        password : bcrypt.hashSync(reqBody.password, 10),
    })
    return newUser.save().then((saved, error) => {
        if(error){
            console.log(error)
            return false
        } else {
            return true
        }
    })
}

// get details
module.exports.getProfile = (userData) => {
	return User.findById( userData.userId ).then(result =>{
        console.log(`user ID ${userData.isAdmin}`)
		if (result === null) {
			return false
		} else {
			result.password = "";
			return result
		}
	})
}

// user authentication
module.exports.checkUser = (reqBody) => {
    return User.find({ email : reqBody.email}).then((result, error) => {
        if (error) {
            console.log(error)
            return false
        } else {
            if (result.length > 0) { 
                return result
            } else {
                return false
            }
        }
    })
}



module.exports.authenticate = (reqBody) => {
    return User.findOne( {email:reqBody.email}).then( result => {
        if (result === null) {
            return false
        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if (isPasswordCorrect) {
                return {access: auth.createAccessToken(result.toObject())}
            } else {
                return false
            }
        }
    })
}


// trial
/* module.exports.setAdmin = (reqBody) => {
    return User.find({ email : reqBody.email}).then((result, error) => {
        if (error) {
            console.log(error)
            return false
        } else {
            if (result.length > 0) {
                console.log(result)
                if(result.isAdmin === false) {
                    return false
                } else {
                    let newAdmin = {
                        isAdmin : true
                    }
                    return User.findByIdAndUpdate(reqBody.userId, updatedUser).then((result, error) => {
                        if (error) {
                            return false
                        } else	{
                            return true
                        }
                    })
                }
                
            } else {
                return false
            }
        }
    })
} */



// set as admin
module.exports.setAdmin = (reqBody, userData, reqParams) => {
    return User.findById(userData.userId).then((result, error) =>{
        console.log(`this is what's inside reqBody`)
        console.log(reqBody)
        console.log(`this is what's inside userData`)
        console.log(userData)
        console.log(`this is what's inside reqParams`)
        console.log(reqParams)
        if (error) {
            return false
        } else {
          if (userData.isAdmin === false) {
              return false
          } else {
              // result.isAdmin = setNewAdmin
              let setNewAdmin = {
                  isAdmin : reqBody.isAdmin
                }
                return User.findByIdAndUpdate(reqParams.userId, setNewAdmin).then((result, error) => {
                    if (error) {
                        return false
                    } else	{
                    return true
                }
            })
          }
        }
    })
}

/* module.exports.setAdmin = (reqBody, userData) => {

    return User.findById(userData.userId).then((result, error) =>{
        if (error) {
            console.log(error)
            return false
        } else {
            if (result.length > 0) {
                if(userData.isAdmin === false){
                    return false
                } else {
                    let newAdmin = {
                        isAdmin : reqBody.isAdmin
                    }
                    return User.findByIdAndUpdate(reqBody.userId, update).then((result, error) => {
                        if (error) {
                            return false
                        } else	{
                            return true
                        }
                    })
                }
            } else {
                return "No such User exist in the Database"
            }
        }
    })
} */





module.exports.enroll = async (data) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {
        user.enrollments.push({courseId:data.courseId})
        
        // saving
        return user.save().then((user, err) => {
        if (err) {
            return false
        } else {
                return true
            }
        })
    })

    let isCourseUpdated = await Course.findById(data.courseId).then(course => {
        course.enrollees.push({userId:data.userId})

        return course.save().then((user, err) => {
            if (err) {
                return false
            } else {
                    return true
                }
            })
    })

    if (isUserUpdated && isCourseUpdated){
        return true
    } else {
        return false
    }
}