const mongoose = require ("mongoose")

const orderSchema = ({

    totalAmount : {
        type : Number,
        required : [true, "Please enter an amount"]
    },
    purchasedOn : {
        type : Date,
        default : new Date()
    },
    isActive : {
        type : Boolean,
        default : true
    },
    isPaid : {
        type : Boolean,
        default : false
    }
})


module.exports = mongoose.model("Order", orderSchema)