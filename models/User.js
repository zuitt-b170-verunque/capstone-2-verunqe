const mongoose = require ("mongoose")

const userSchema = ({
    email:{
        type:String,
        required:[true, "Please input your email"]
    },
    password:{
        type: String,
        required:[true, "Password is required"]
    },

    isAdmin : {
        type : Boolean,
        default : false
    },
    cart :[ {
        productId : {
            type: String,
			required: [false, " ID is required"]
        },
        amount : {
            type : Number,
            required : [false, "Amount is required"]
        },
        purchasedOn : {
            type : Date,
            default : new Date()
        },
        isPaid : {
            type : Boolean,
            default : false
        }
    }]
})


module.exports = mongoose.model("User", userSchema);