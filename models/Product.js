const mongoose = require ("mongoose")

const productSchema = ({
    name : {
        type : String,
        required : [true, "Please input the prudct name"]
    },
    description : {
        type : String,
        required : [true, "Please input a description of the product"]
    },
    price : {
        type : Number,
        required : [true, "Please input the price of the product"]
    },
    isActive : {
        type : Boolean,
        default : true
    },
    amount : {
        type : Number,
        default : 1
    }
})

module.exports = mongoose.model("Product", productSchema);