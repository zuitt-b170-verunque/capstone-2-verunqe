const express = require ("express")
const router = express.Router()

const auth = require ("../auth.js")
const orderController = require ("../controllers/orderController.js")



// NON-ADMIN USER CHECK-OUT
// router.post("/:userId/order", auth.verify, (req, res) => {
router.post("/order", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    orderController.order(req.body, userData).then(result => res.send (result))
})

// ADD TO CART
router.post("/cart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    orderController.cart(req.body, userData).then(result => res.send (result))
})



// RETRIEVE AUTHENTICATED USER'S ORDER
router.get("/retrieveorder", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    orderController.retrieveOrder(req.body, userData).then(result => res.send (result))
})


// RETRIEVE ALL ORDERS (ADMIN)
router.get("/retrieveall", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    orderController.retrieveAllOrder( userData).then(result => res.send(result))
})


module.exports = router