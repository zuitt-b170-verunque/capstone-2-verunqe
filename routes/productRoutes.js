const express = require ("express")
const router = express.Router()
const auth = require ("../auth.js")

const productController = require ("../controllers/productController.js")



// GET ALL ACTIVE PRODUCTS
router.get("/getactive", auth.verify, (req, res) => {
    productController.getActiveProducts().then(result => res.send(result))
})

// GET ALL
router.get("/", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})


// GET A SINGLE PRODUCT
router.get("/:productId", (req, res) => {
    productController.getProduct(req.params.productId).then(result => {
        res.send(result)
    })
})

// CREATE A PRODUCT (ADMIN PRIVILAGE)
router.post("/additem", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.addItem(req.body, userData).then(result => res.send (result))
})

// UPDATE A PRODUCT
router.put("/:productId", (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.updateProduct(req.params, req.body, userData).then(result => res.send(result))
})

// ARCHIVE PRODUCT
router.put("/:productId/archiveProduct", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
	productController.archiveProduct(req.params, req.body, userData).then(result => res.send(result))
})














module.exports = router