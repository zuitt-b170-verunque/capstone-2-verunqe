const express = require ("express")
const router = express.Router()

const auth = require ("../auth.js")
const userController = require ("../controllers/userController.js")

const User = require ("../models/User.js")


// user registration
router.post("/register", (req, res) =>{
    userController.registerUser(req.body).then(result => res.send (result))
})

// user authentication
router.post("/userauthentication", (req, res)=> {
    userController.authenticate(req.body).then(result => res.send (result))
})

// MISC
router.post("/checkUser", (req, res)=> {
    userController.checkUser(req.body).then(result => res.send (result))
})

// set user as admin (admin privilege)
router.put("/:userId/setAdmin", auth.verify, (req, res) => {
    // save the user data that is inside headers >> authorization >> bearer token and save it inside userData variable
    const userData = auth.decode(req.headers.authorization)

    userController.setAdmin(req.body, userData, req.params).then(result => res.send(result))
})

// deatails
router.get("/details", auth.verify, (req, res)=> {
	// decode - decrypts the token inside the authorization (which is in the headers of the request) 
	// req.headers.authorization contains the token that was created for the user
	const userData = auth.decode(req.headers.authorization)
    console.log(userData)
	userController.getProfile( {userId: userData.id} ).then(resultFromController => res.send(resultFromController))
} )






module.exports = router