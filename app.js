// dependency
const express = require("express")
const mongoose = require ("mongoose")
const cors = require ("cors")


// access to routes
const userRoutes = require ("./routes/userRoutes.js")
const productRoutes = require ("./routes/productRoutes.js")
const orderRoutes = require ("./routes/orderRoutes.js")

// server
const app = express()
const port = 4000

// the other things (app.use)
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))


// database connection
mongoose.connect("mongodb+srv://verunque_david:password1210@wdc028-course-booking.13hym.mongodb.net/b170-verunque-ecommerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
	useUnifiedTopology:true
})

// the inital part of URI
app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/orders", orderRoutes);


// creating variable for database
let db = mongoose.connection;
db.on("error", console.error.bind("Connection Error!"))
db.on("open", () => console.log("We're connected to the database") )


// listen
app.listen(process.env.PORT||port, () => console.log(`Successfully connected to port ${process.env.PORT || port} `))

// heroku
